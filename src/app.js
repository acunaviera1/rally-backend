import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'

// import initializeDb from './db'
// import api from './api'
import config from './config.json'
import Router from './api/routes'

const app = express();

app.use(morgan('dev'))
app.use(cors({
  exposedHeaders: config.corsHeaders
}))

app.use(bodyParser.json({
  limit: config.bodyLimit
}))

app.use(bodyParser.urlencoded({ extended: false }))

Router(app)

app.get('*', (req, res) => res.status(200).send({
  message: 'nope.',
}))

export default app