import parser from '../lib/parser'
import db from '../db/models/'

  const parseTrophies = () => {
    return new Promise((resolve, reject) => {
      parser.queryTrophies().then((response) => {
        parser.saveTrophy(response).then(res => {
          resolve(res)
        }).catch(err => {
          console.log(err)
          reject(err)
        })
      })
      .catch((err) => {
        console.log(err)
        reject(err)
      })
    })
  }

  const parsePrimes = (trophy) =>{
    return new Promise((resolve, reject) => {
      parser.queryPrimes().then((response) => {
        parser.savePrimes(response, trophy).then((res) => {
          resolve(res)
        })
      }).catch((err) => {
        console.log(err)
        reject(err)
      })
    })
  }

const processDatabase = (erase) => {
  return new Promise((resolve, reject) => {
    if(erase){
      db.sequelize.query('DROP TABLE IF EXISTS `Positionbackups`;').spread((results, metadata) => {
        console.log('1111111111111111')
        console.log(results)
        console.log(metadata)
        console.log('1111111111111111 \n \n')
        // create table
        db.sequelize.query('CREATE TABLE  `Positionbackups` LIKE Positions;').spread((results, metadata) => {
          console.log('222222222222222')
          console.log(results)
          console.log(metadata)
          console.log('222222222222222 \n \n')
          //make backup
          db.sequelize.query('INSERT `Positionbackups` SELECT * FROM Positions;').spread((results, metadata) => {
            console.log('3333333333333333')
            console.log(results)
            console.log(metadata)
            console.log('3333333333333333 \n \n')
            // empty position database
            db.sequelize.query('TRUNCATE TABLE Positions;').spread((results, metadata) => {
              console.log('4444444444444444')
              console.log(results)
              console.log(metadata)
              console.log('4444444444444444 \n \n')
              resolve(results)
            }).catch(err => reject(err))
          }).catch(err => reject(err))
        }).catch(err => reject(err))
      }).catch(err => reject(err))
    } else {
      console.log('5555555555555555')
      console.log('empty position table but backup, keep on and try parsing')
      console.log('5555555555555555 \n \n')
      resolve()
    }
  })
}

const parsePrime = (prime, type) => {
  return new Promise((resolve, reject) => {
    parser.queryPositions(prime.dataValues, type).then(response => {
      parser.savePositions(response).then(poss => resolve(poss))
      .catch(err => reject(err))
    }).catch(err => reject(err))
  })
}



  const parsePositions = () =>{
    return new Promise((resolve, reject) => {
      parseTrophies().then(trophy => {
        parsePrimes(trophy).then(primes => {
          parser.checkPositionEmptyness().then(erase => {
            processDatabase(erase).then(() => {
              const types = ['tiemposAcumulados', 'tiemposPrime']
              const tasks = []
              types.map(type => {
                primes.map(prime => {
                  tasks.push(parsePrime(prime, type))
                })
              })
              tasks.reduce((promiseChain, currentTask) => {
                return promiseChain.then(chainResults =>
                  currentTask.then(currentResult =>
                    [...chainResults, currentResult]
                  )
                )
              }, Promise.resolve([])).then(arrayOfResults => {
                parser.saveParseHistory('ok').then(() => {
                  resolve(arrayOfResults)
                })
              })
            })
          }).catch(err => {
            parser.saveParseHistory('error').then(() => {
              reject(err)
            })
          })
        })
      })
    })
  }

  export default {
    parseTrophies,
    parsePrimes,
    parsePositions
  }