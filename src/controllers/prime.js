import Models from '../db/models/'

export default {
  create(req, res) {
    return Models.Prime
      .create({
        title: req.body.title,
        value: req.body.value
      })
      .then(prime => res.status(201).send(prime))
      .catch(error => res.status(400).send(error))
  },
  list(req, res) {
    return Models.Prime
      .all()
      .then(primes => res.status(200).send(primes))
      .catch(error => res.status(400).send(error))
  }
}