
import Models from '../db/models'

export default {
  create(req, res) {
    return Models.Brand
      .create({
        name: req.body.name,
        slug: req.body.slug
      })
      .then(brand => res.status(201).send(brand))
      .catch(error => res.status(400).send(error))
  },
  list(req, res) {
    return Models.Brand
      .all()
      .then(brands => res.status(200).send(brands))
      .catch(error => res.status(400).send(error))
  }
}
