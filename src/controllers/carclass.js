import Models from '../db/models/'

export default {
  create(req, res) {
    return Models.Carclass
      .create({
        name: req.body.name,
        slug: req.body.slug,
        value: req.body.value
      })
      .then(carclass => res.status(201).send(carclass))
      .catch(error => res.status(400).send(error))
  },
  list(req, res) {
    return Models.Carclass
      .all()
      .then(carclasses => res.status(200).send(carclasses))
      .catch(error => res.status(400).send(error))
  }
}