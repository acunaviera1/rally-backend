import Brand from '../controllers/brand'
import Carclass from '../controllers/carclass'
import Position from '../controllers/position'
import Prime from '../controllers/prime'
import Trophy from '../controllers/trophy'

import Parser from '../controllers/parser'

export default {
  Brand,
  Carclass,
  Position,
  Prime,
  Trophy,
  Parser
}