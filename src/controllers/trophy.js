import Models from '../db/models/'

export default {
  create(req, res){
    return Models.Trophy
      .create({
        title: req.body.title,
        undertitle: req.body.undertitle
      })
      .then(trophy => res.status(201).send(trophy))
      .catch(error => res.status(400).send(error))
  },
  list(req, res) {
    return Models.Trophy
      .all()
      .then(trophies => res.status(200).send(trophies))
      .catch(error => res.status(400).send(error))
  }
}