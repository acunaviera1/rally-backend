import Models from '../db/models/'

export default {
  create(req, res) {
    return Models.Position
      .create({
        slug: req.body.slug,
        type: req.body.name,
        position: req.body.position,
        carnumber: req.body.carnumber,
        pilots: req.body.pilots,
        nettime: req.body.nettime,
        averagespeed: req.body.averagespeed,
        totaltime: req.body.totaltime
      })
      .then(position => res.status(201).send(position))
      .catch(error => res.status(400).send(error))
  },
  list(req, res) {
    return Models.Position
      .all()
      .then(positions => res.status(200).send(positions))
      .catch(error => res.status(400).send(error))
  }
}