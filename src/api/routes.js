import { version } from '../../package.json'
import Controllers from '../controllers/'
var mcache = require('memory-cache');

var cache = (duration) => {
  return (req, res, next) => {
    let key = '__express__' + req.originalUrl || req.url
    let cachedBody = mcache.get(key)
    if (cachedBody) {
      const resp = JSON.parse(cachedBody)
      res.json(resp)
      return
    } else {
      res.sendResponse = res.send
      res.send = (body) => {
        console.log(body)
        mcache.put(key, body, duration * 1000);
        res.sendResponse(body)
      }
      next()
    }
  }
}

export default function( app ) {
  // app.get('/', cache(20), (req, res) => {
  //   Controllers.Position.create(req, res)
  // })

  // // app.post('/api/positions', Controllers.Position.create)
  // // app.post('/api/primes', Controllers.Prime.create)
  // app.get('/', cache(20), (req, res) => {
  //   Controllers.Prime.create(req, res)
  // })

  // // app.post('/api/trophies', Controllers.Trophy.create)

  // app.get('/', cache(20), (req, res) => {
  //   Controllers.Trophy.create(req, res)
  // })

  // app.get('/api/positions', Controllers.Position.list)
  // app.get('/api/primes', Controllers.Prime.list)
  // app.get('/api/trophies', Controllers.Trophy.list)

  app.get('/api/positions', cache(900), (req, res) => {
    Controllers.Position.list(req, res)
  })

  app.get('/api/primes', cache(900), (req, res) => {
    Controllers.Prime.list(req, res)
  })

  app.get('/api/trophies', cache(900), (req, res) => {
    Controllers.Trophy.list(req, res)
  })

  app.post('/parser/trophies', (req, res) => {
    Controllers.Parser.parseTrophies()
      .then(response => res.json(response))
  })
  app.post('/parser/positions', (req, res) => {
    Controllers.Parser.parsePositions()
      .then(response => res.json(response))
  })

  app.get('/api', (req, res) => {
    res.json({ version })
  })
}
