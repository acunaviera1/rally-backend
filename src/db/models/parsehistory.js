module.exports = (sequelize, DataTypes) => {
  const Parsehistory = sequelize.define('Parsehistory', {
    result: {
      type: DataTypes.TEXT
    },
  }, {});
  Parsehistory.associate = function (models) {
    // associations can be defined here
  }
  return Parsehistory
}