module.exports = (sequelize, DataTypes) => {
  const Trophy = sequelize.define('Trophy', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    undertitle: DataTypes.STRING,
    dateid: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {})
  Trophy.associate = (models) => {
    Trophy.hasMany(models.Prime)
  }
  return Trophy
};