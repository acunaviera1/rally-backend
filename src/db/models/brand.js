module.exports = function(sequelize, DataTypes) {
  const Brand = sequelize.define('Brand', {
    name: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    slug: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {})
  Brand.associate = (models) => {
    // associations can be defined here
  }
  return Brand
}