module.exports = (sequelize, DataTypes) => {
  const Carclass = sequelize.define('Carclass', {
    name: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    slug: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {});
  Carclass.associate = function(models) {
    // associations can be defined here
  }
  return Carclass
}