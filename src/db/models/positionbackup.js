module.exports = (sequelize, DataTypes) => {
  const Positionbackup = sequelize.define('Positionbackup', {
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    position: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    carnumber: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    pilots: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    nettime: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    totaltime: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    penalty: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    difference1: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    diffbefore: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    averagespeed: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    brand: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    carclass: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {})
  Positionbackup.associate = function(models) {
    Positionbackup.belongsTo(models.Prime, {
      onDelete: 'CASCADE'
    })
  }
  return Positionbackup
}