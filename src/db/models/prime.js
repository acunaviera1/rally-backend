'use strict';
module.exports = (sequelize, DataTypes) => {
  const Prime = sequelize.define('Prime', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {});
  Prime.associate = (models) => {
    Prime.belongsTo(models.Trophy, {
      onDelete: 'CASCADE'
    })
    Prime.hasMany(models.Position)
  }
  return Prime
};