'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "type" on table "Positions"
 * changeColumn "name" on table "Brands"
 * changeColumn "name" on table "Carclasses"
 * changeColumn "slug" on table "Carclasses"
 * changeColumn "value" on table "Carclasses"
 * changeColumn "slug" on table "Positions"
 * changeColumn "slug" on table "Brands"
 * changeColumn "position" on table "Positions"
 * changeColumn "carnumber" on table "Positions"
 * changeColumn "pilots" on table "Positions"
 * changeColumn "nettime" on table "Positions"
 * changeColumn "averagespeed" on table "Positions"
 * changeColumn "totaltime" on table "Positions"
 *
 **/

var info = {
    "revision": 2,
    "name": "validations",
    "created": "2018-10-17T18:46:01.628Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "changeColumn",
        params: [
            "Positions",
            "type",
            {
                "type": Sequelize.STRING,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Brands",
            "name",
            {
                "type": Sequelize.TEXT,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Carclasses",
            "name",
            {
                "type": Sequelize.TEXT,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Carclasses",
            "slug",
            {
                "type": Sequelize.STRING,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Carclasses",
            "value",
            {
                "type": Sequelize.STRING,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "slug",
            {
                "type": Sequelize.STRING,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Brands",
            "slug",
            {
                "type": Sequelize.STRING,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "position",
            {
                "type": Sequelize.INTEGER,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "carnumber",
            {
                "type": Sequelize.INTEGER,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "pilots",
            {
                "type": Sequelize.TEXT,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "nettime",
            {
                "type": Sequelize.TEXT,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "averagespeed",
            {
                "type": Sequelize.TEXT,
                "allowNull": false
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "totaltime",
            {
                "type": Sequelize.TEXT,
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
