'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "Parsehistories", deps: []
 *
 **/

var info = {
    "revision": 10,
    "name": "ParseHistory",
    "created": "2018-10-19T14:53:42.202Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "createTable",
    params: [
        "Parsehistories",
        {
            "id": {
                "type": Sequelize.INTEGER,
                "autoIncrement": true,
                "primaryKey": true,
                "allowNull": false
            },
            "createdAt": {
                "type": Sequelize.DATE,
                "allowNull": false
            },
            "updatedAt": {
                "type": Sequelize.DATE,
                "allowNull": false
            }
        },
        {}
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
