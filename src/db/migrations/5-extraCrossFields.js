'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "BrandId" from table "Positions"
 * removeColumn "CarclassId" from table "Positions"
 * addColumn "carclass" to table "Positions"
 * addColumn "brand" to table "Positions"
 *
 **/

var info = {
    "revision": 5,
    "name": "extraCrossFields",
    "created": "2018-10-18T21:17:15.993Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "removeColumn",
        params: ["Positions", "BrandId"]
    },
    {
        fn: "removeColumn",
        params: ["Positions", "CarclassId"]
    },
    {
        fn: "addColumn",
        params: [
            "Positions",
            "carclass",
            {
                "type": Sequelize.STRING,
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "Positions",
            "brand",
            {
                "type": Sequelize.TEXT,
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
