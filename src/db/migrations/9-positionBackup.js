'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "Positionbackups", deps: [Primes]
 *
 **/

var info = {
    "revision": 9,
    "name": "positionBackup",
    "created": "2018-10-19T13:27:02.528Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "createTable",
    params: [
        "Positionbackups",
        {
            "id": {
                "type": Sequelize.INTEGER,
                "autoIncrement": true,
                "primaryKey": true,
                "allowNull": false
            },
            "type": {
                "type": Sequelize.STRING,
                "allowNull": false
            },
            "position": {
                "type": Sequelize.INTEGER,
                "allowNull": false
            },
            "carnumber": {
                "type": Sequelize.INTEGER,
                "allowNull": false
            },
            "pilots": {
                "type": Sequelize.TEXT,
                "allowNull": false
            },
            "nettime": {
                "type": Sequelize.TEXT,
                "allowNull": true
            },
            "totaltime": {
                "type": Sequelize.TEXT,
                "allowNull": true
            },
            "penalty": {
                "type": Sequelize.TEXT,
                "allowNull": true
            },
            "difference1": {
                "type": Sequelize.TEXT,
                "allowNull": true
            },
            "diffbefore": {
                "type": Sequelize.TEXT,
                "allowNull": true
            },
            "averagespeed": {
                "type": Sequelize.TEXT,
                "allowNull": true
            },
            "brand": {
                "type": Sequelize.TEXT,
                "allowNull": false
            },
            "carclass": {
                "type": Sequelize.STRING,
                "allowNull": false
            },
            "createdAt": {
                "type": Sequelize.DATE,
                "allowNull": false
            },
            "updatedAt": {
                "type": Sequelize.DATE,
                "allowNull": false
            },
            "PrimeId": {
                "type": Sequelize.INTEGER,
                "onUpdate": "CASCADE",
                "onDelete": "CASCADE",
                "references": {
                    "model": "Primes",
                    "key": "id"
                },
                "allowNull": true
            }
        },
        {}
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
