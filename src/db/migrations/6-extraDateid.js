'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "dateid" to table "Trophies"
 *
 **/

var info = {
    "revision": 6,
    "name": "extraDateid",
    "created": "2018-10-18T22:33:53.439Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "addColumn",
    params: [
        "Trophies",
        "dateid",
        {
            "type": Sequelize.STRING,
            "allowNull": false
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
