'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "diffbefore" to table "Positions"
 * addColumn "difference1" to table "Positions"
 * addColumn "penalty" to table "Positions"
 * changeColumn "totaltime" on table "Positions"
 * changeColumn "averagespeed" on table "Positions"
 * changeColumn "nettime" on table "Positions"
 *
 **/

var info = {
    "revision": 4,
    "name": "extraPositionFields",
    "created": "2018-10-18T01:37:04.281Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "addColumn",
        params: [
            "Positions",
            "diffbefore",
            {
                "type": Sequelize.TEXT,
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "Positions",
            "difference1",
            {
                "type": Sequelize.TEXT,
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "Positions",
            "penalty",
            {
                "type": Sequelize.TEXT,
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "totaltime",
            {
                "type": Sequelize.TEXT,
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "averagespeed",
            {
                "type": Sequelize.TEXT,
                "allowNull": true
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Positions",
            "nettime",
            {
                "type": Sequelize.TEXT,
                "allowNull": true
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
