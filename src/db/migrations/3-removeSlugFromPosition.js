'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "slug" from table "Positions"
 *
 **/

var info = {
    "revision": 3,
    "name": "removeSlugFromPosition",
    "created": "2018-10-18T00:56:10.195Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "removeColumn",
    params: ["Positions", "slug"]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
