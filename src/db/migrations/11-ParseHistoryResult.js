'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "result" to table "Parsehistories"
 *
 **/

var info = {
    "revision": 11,
    "name": "ParseHistoryResult",
    "created": "2018-10-19T14:56:16.315Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "addColumn",
    params: [
        "Parsehistories",
        "result",
        {
            "type": Sequelize.TEXT
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
