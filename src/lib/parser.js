
import Xray from 'x-ray'
import Models from '../db/models/'
const path = require('path')
const s = require('underscore.string');

const x = Xray({
  filters: {
    clean: function (value) {
      return typeof value === 'string' ? s.clean(value) : value
    },
    removepath: function (value) {
      // http://anarerally.cl/Resultados/images/logos/citroen.png
      return path.basename(value, path.extname(value))
    }
  },
})

x.throttle(1, 1000);

const queryTrophies = () => {
  return new Promise((resolve, reject) => {
    const url = 'http://anarerally.cl/Resultados/tiemposPrime.php'
    x(url, '.TXT_TIEMPO_TITULO.nombrePrueba', [{
      strtext: '@html',
    }])((err, response) => {
      if (err)
        reject(err)
      x(url, 'form input[name="date"]', [{
        dateid: '@value'
      }])((err, res) => {
        if (err)
          reject(err)
        try {
          if (response[0] && response[0].strtext && res[0] && res[0].dateid) {
            const str = []
            response[0].strtext.split('<br>').map((st) => {
              str.push(st.trim())
            })
            resolve({
              title: str[0],
              undertitle: str[1],
              dateid: res[0].dateid
            })
          } else {
            reject('empty')
          }
        } catch (err) {
          reject(err)
        }
      })
    })
  })
}

const queryPrimes = () => {
  return new Promise((resolve, reject) => {
    const url = 'http://anarerally.cl/Resultados/tiemposPrime.php'

    x(url, 'select[name="cboPrueba"] option', [{
      title: '@text',
      value: '@value'
    }])((err, response) => {
      if (err)
        reject(err)
      resolve(response)
    })
  })
}

const addExtras = (position, prime, type) => {
  return new Promise((resolve, reject) => {
    position.PrimeId = prime.id
    position.type = type
    resolve(position)
  })
}

const saveParseHistory = (result) => {
  return new Promise((resolve, reject) => {
    Models.Parsehistory.create({
      result: result
    }).then(response => resolve(response))
      .catch(err => reject(err))
  })
}

const queryPositions = (prime, type = "tiemposAcumulados") => {
  return new Promise((resolve, reject) => {
    const url = `http://anarerally.cl/Resultados/${type}.php?cboPrueba=${prime.value}`

    const pdata = {
      tiemposAcumulados: {
        position: 'td.resultadoPosicion span@text|clean',
        carnumber: 'td:nth-child(2) span@text|clean',
        pilots: 'td:nth-child(3)@text|clean',
        totaltime: 'td:nth-child(7)@text|clean',
        penalty: 'td:nth-child(8)@text|clean',
        difference1: 'td:nth-child(9)@text|clean',
        diffbefore: 'td:nth-child(10)@text|clean',
        averagespeed: 'td:nth-child(11)@text|clean',
        carclass: 'td:nth-child(5)@text|clean',
        brand: 'td:nth-child(4) img@src|clean|removepath',
      },
      tiemposPrime: {
        position: 'td.resultadoPosicion span@text|clean',
        carnumber: 'td:nth-child(2) span@text|clean',
        pilots: 'td:nth-child(3)@text|clean',
        nettime: 'td:nth-child(7)@text|clean',
        difference1: 'td:nth-child(8)@text|clean',
        diffbefore: 'td:nth-child(9)@text|clean',
        averagespeed: 'td:nth-child(10)@text|clean',
        carclass: 'td:nth-child(5)@text|clean',
        brand: 'td:nth-child(4) img@src|clean|removepath',
      },
    }

    x(url, 'table.titulos tbody tr', [pdata[type]])((err, response) => {
      if (err)
        reject(err)
      const tasks = []
      // console.log(response)
      response.map(position => {
        tasks.push(addExtras(position, prime, type))
      })

      tasks.reduce((promiseChain, currentTask) => {
        return promiseChain.then(chainResults =>
          currentTask.then(currentResult =>
            [...chainResults, currentResult]
          )
        )
      }, Promise.resolve([])).then(arrayOfResults => {
        resolve(arrayOfResults)
      })
    })
  })
}

const checkPositionEmptyness = () => {
  return new Promise((resolve, reject) => {
    Models.Position.count().then(p => {
      if(p === 0){
        Models.Positionbackup.count().then(pb => {
          if(pb === 0){
            //first load? ok
            resolve(true)
          } else {
            //theres backup but no positions, dont erase backup
            resolve(false)
          }
        }).catch(err => reject(err))
      } else {
        resolve(true)
      }
    }).catch(err => reject(err))
  })
}

const savePositions = (positions) => {
  return new Promise((resolve, reject) => {
    Models.Position.bulkCreate(positions)
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err)
      })
  })
}

const savePrime = (prime, trophy) => {
  return new Promise((resolve, reject) => {
    Models.Prime.findOrCreate({
      where: {
        title: prime.title,
        value: prime.value,
        TrophyId: trophy.dataValues.id
      },
      defaults: {
        title: prime.title,
        value: prime.value,
        TrophyId: trophy.dataValues.id
      }
    })
    .spread((prime) => {
      resolve(prime)
    })
    .catch(err => reject(err))
  })
}

const savePrimes = (primes, trophy) => {
  return new Promise((resolve, reject) => {
    const tasks = []

    primes.map(prime => {
      tasks.push(savePrime(prime, trophy))
    })

    tasks.reduce((promiseChain, currentTask) => {
      return promiseChain.then(chainResults =>
        currentTask.then(currentResult =>
          [...chainResults, currentResult]
        )
      )
    }, Promise.resolve([])).then(arrayOfResults => {
      resolve(arrayOfResults)
    })
    .catch(err => reject(err))
  })
}

const saveTrophy = (trophy) => {
  return new Promise((resolve, reject) => {
    Models.Trophy.findOrCreate({
      where: {
        title: trophy.title
      },
      defaults: {
        title: trophy.title,
        undertitle: trophy.undertitle,
        dateid: trophy.dateid
      }
    })
    .spread((trophy) => {
      resolve(trophy)
    }).catch(err => {
      reject(err)
    })
  })
}

export default {
  queryPrimes,
  savePrimes,
  queryTrophies,
  saveTrophy,
  queryPositions,
  savePositions,
  checkPositionEmptyness,
  saveParseHistory
}